FROM maven:alpine
 
COPY . ./usr/local/app

EXPOSE 8080
ENTRYPOINT ["mvn", "clean","package","jetty:run-forked"]